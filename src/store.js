import Vue from 'vue'
import  Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        // 所有任务项数据
        list: [],
        // 添加任务框内容
        inputVal: '',
        id: 5,
        // 菜单切换key
        viewKey: 'all'
    },
    mutations: {
        // 把接收到的list数据赋值到state 
        saveListData(state, data) {
            state.list = data
        },
        // 选择任务项状态
        checkTask(state, params) {
            // 寻找当前被点击任务项在list中的索引
            let index = state.list.findIndex(x => x.id === params.id)
            // 修改当前被点击任务项在list中的状态
            state.list[index].done = params.checked
        },
        // 同步输入框的值到state
        syncAddInput(state, val) {
            state.inputVal = val
        },
        // 添加任务项
        addTask(state) {
            // 构造任务项的数据结构
            let task = {
                info: state.inputVal,
                id: state.id,
                done: false
            }

            state.id++
            state.inputVal = ''
            
            state.list.push(task)
        },
        // 删除任务项
        delTask(state, id) {
            // 获取需要删除任务项的索引
            let index = state.list.findIndex(x => x.id === id)
            state.list.splice(index, 1)
            
        },
        // 删除所有已经完成的任务项
        delDonedAll(state) {
            state.list = state.list.filter(x => x.done===false)
        },
        // 切换菜单
        switchMenu(state, viewKey) {
            state.viewKey = viewKey
        }
    },
    actions: {
        // 获取list数据
        initListData(context) {
            // 因为axios是异步的操作，所以需要放在actions中发送请求获取数据，然后再调用mutation存储值到state的list中
            axios.get('list.json').then(({data}) => {
                context.commit('saveListData', data)
            })
        }
    },
    getters: {
        // 获取剩余未完成的任务项
        undoneShow(state) {
            return state.list.filter(x => x.done === false).length
        },
        switchMenuInfo(state) {
            if (state.viewKey === 'done') {
                return state.list.filter(x => x.done === true)
            } else if (state.viewKey === 'undone') {
                return state.list.filter(x => x.done === false)
            } else {
                return state.list
            }
        }
    }
})
